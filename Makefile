#Configure flags etc.
CFLAGS :=
CXXFLAGS := -std=c++17 -fopenmp -O3
PROGNAME :=vkcompute

SRC_LIBS := 
LIBS := vulkan z SPIRV-Tools

# Finding source files
C_FILES := $(shell find src/ -name "*.cpp" -or -name "*.cc" -or -name "*.c" -and -not -name "*_flymake.cpp")# | sed ':a;N;$!ba;s/\n/ /g')
L_FILES := $(shell find src/ -name "*.l")
Y_FILES := $(shell find src/ -name "*.y")

COMP_SHADER_FILES:=$(shell find . -name "*.comp")
OPENCL_FILES := $(shell find . -name "*.cl")

INCLUDE_DIRS := src/ ./generated/src/ ./include/
LIBRARY_DIRS := ./lib/

# Toolchain-setup
CC := gcc
CXX := g++
LD := ld
AR := ar
LEX := flex
YACC := bison
AS := as

SPIRV_COMP := ./tools/glslangValidator
OPENCL_COMP := ./tools/clspv

# Functions used for target-generation
get_dependency = $(shell ${1} | g++ -MM $(addprefix -I, ${INCLUDE_DIRS}) -x c++ - | sed -e ':a;N;$$!ba;s/\\\n //g' | sed -e 's/[A-Za-z\.\/_-]*: //')
obj_target=obj/${2}/$(addsuffix .o,$(basename ${1}))
lexer_target=generated/$(basename ${1}).scanner.cpp
parser_target=generated/$(basename ${1}).parser.cpp
srclib_target=srclibs/${1}/lib/lib${1}.a
shader_target=${1}.spirv
cl_target=${1}.spirv

# List of files that will produce an object-file
# without needing any targets to be produced.
GENERATED_C_FILES := 

define obj
$(call obj_target,${1},${2}) : ${1} $(call get_dependency,cat ${1}) | obj/Debug/
obj$(suffix ${1}) += $(call obj_target,${1},${2})
endef

define lexer
$(call lexer_target,${1}) : ${1} | generated/
scanner$(suffix ${1})+= $(call lexer_target,${1})

$(call generated_obj,$(call lexer_target,${1}),Debug,${LEX} -t ${1})

GENERATED_C_FILES += ${call lexer_target,${1}}
endef

define parser
$(call parser_target,${1}) : ${1} | generated/
parser$(suffix ${1}) += $(call parser_target,${1})

$(call generated_obj,$(call parser_target,${1}),Debug,${YACC} -t ${1})

GENERATED_C_FILES += ${call parser_target,${1}}
endef

define srclib
$(call srclib_target,${1}) : srclibs/${1}/
srclibs+=$(call srclib_target,${1})
endef

define shader
$(call shader_target,${1}) : ${1} |
shaders.spirv+=$(call shader_target,${1})
endef

define opencl
$(call cl_target,${1}) : ${1} |
opencl_modules.spirv+=$(call cl_target,${1})
endef

define generated_obj
$(call obj_target,${1},${2}) : ${1} $(call get_dependency,${3}) | obj/Debug/
obj$(suffix ${1}) += $(call obj_target,${1},${2})
endef

all: Debug

run: Debug
	./bin/Debug/vkengine

Debug: CFLAGS +=-g -rdynamic -DDEBUG
Debug: CXXFLAGS +=-g -rdynamic -DDEBUG
Debug: bin/Debug/${PROGNAME}

Logging: CFLAGS +=-g -rdynamic -DDEBUG -DDEBUG_LOGGING
Logging: CXXFLAGS +=-g -rdynamic -DDEBUG -DDEBUG_LOGGING
Logging: bin/Debug/${PROGNAME}

DebugW: CFLAGS +=-g -rdynamic -DDEBUG -Werror
DebugW: CXXFLAGS +=-g -rdynamic -DDEBUG -Werror
DebugW: bin/Debug/${PROGNAME}

Release: CFLAGS +=-O3
Release: CXXFLAGS += -O3
Release: bin/Release/${PROGNAME}

Library: lib/lib${PROGNAME}.a

# Files that need preprocessing

$(foreach y,${Y_FILES},$(eval $(call parser,${y})))
$(foreach l,${L_FILES},$(eval $(call lexer,${l})))


$(foreach src,${C_FILES},$(eval $(call obj,${src},Debug)))
$(foreach lib,${SRC_LIBS},$(eval $(call srclib,${lib})))

$(foreach shdr,${COMP_SHADER_FILES},$(eval $(call shader,${shdr})))
$(foreach shdr,${OPENCL_FILES},$(eval $(call opencl,${shdr})))

O_FILES:=$(foreach src,${C_FILES},$(call obj_target,${src},Debug)) $(foreach src,${GENERATED_C_FILES},$(call obj_target,${src},Debug))
LIBRARY_O_FILES := $(filter-out $(call obj_target,src/main.cpp,Debug),${O_FILES})
SHADER_SPIRVS := $(foreach shdr,${COMP_SHADER_FILES},$(call shader_target,${shdr}))
OPENCL_SPIRVS := $(foreach shdr,${OPENCL_FILES},$(call cl_target,${shdr}))

#Template targets

bin/Debug/${PROGNAME}: ${O_FILES} | bin/Debug/ ${SRC_LIB_ARCHS} ${SHADER_SPIRVS} ${OPENCL_SPIRVS}
	@mkdir -p bin/Debug
	@echo Linking $@
	@$(CXX) -o $@ $^ $(addprefix -L,${LIBRARY_DIRS}) $(addprefix -l, ${LIBS}) $(addprefix -l, ${SRC_LIBS}) $(CXXFLAGS)

bin/Release/${PROGNAME}: ${O_FILES} | bin/Release/ ${SRC_LIB_ARCHS} ${SHADER_SPIRVS} ${OPENCL_SPIRVS}
	@mkdir -p bin/Debug
	@echo Linking $@
	@$(CXX) -o $@ $^ $(addprefix -L,${LIBRARY_DIRS}) $(addprefix -l, ${LIBS}) $(addprefix -l, ${SRC_LIBS}) $(CXXFLAGS)

lib/lib${PROGNAME}.a: ${LIBRARY_O_FILES} | lib/
	@echo Creating library
	@$(AR) -rcs $@ $^

${obj.c} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(CC) -c -o $@ $< $(CFLAGS) $(addprefix -I, ${INCLUDE_DIRS})

${obj.cpp} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(CXX) -c -o $@ $< $(CXXFLAGS) $(addprefix -I, ${INCLUDE_DIRS})

${obj.cc} : % :
	@echo Compiling $@
	@mkdir -p $(dir $@)
	@$(CXX) -c -o $@ $< $(CXXFLAGS) $(addprefix -I, ${INCLUDE_DIRS})

${scanner.l} : % :
	@echo Generating lexer $@
	@mkdir -p $(dir $@)
	@$(LEX) -o $@ $^

${parser.y} : % :
	@echo Generating parser $@
	@mkdir -p $(dir $@)
	@bison -L c++ -v -d $^ -o $@

${srclibs} : % :
	cd srclibs/$(word 2,$(subst /, ,$@))/ && $(MAKE) Library

${shaders.spirv} : % :
	@echo Compiling shader $@
	@$(SPIRV_COMP) -V $< -o $@

${opencl_modules.spirv} : % :
	@echo Compiling shader $@
	@$(OPENCL_COMP) --fp64 $< -o $@

obj/Debug/:
	@mkdir -p obj/Debug/

bin/Debug/:
	@mkdir -p bin/Debug/

bin/Release/:
	@mkdir -p bin/Release/

generated/:
	@mkdir -p generated/

lib/:
	@mkdir -p lib/

cleanDebug: clean

cleanRelease: clean

clean:
	@rm -r obj
	@rm -r bin
