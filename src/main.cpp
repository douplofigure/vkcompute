#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include "vulkan/util.h"
#include "vulkan/gpubuffer.h"
#include "vulkan/gpuimage.h"
#include "vulkan/computeprogram.h"
#include "vulkan/computemodule.h"

#include "util/image/png.h"
#include "vulkan/imageutils.h"
#include "debug/trace_exception.h"

#include <fstream>
#include <chrono>
#include <math.h>
#include <complex>
#include <string.h>

#if DEBUG
const std::vector<const char*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"
};
#else
const std::vector<const char*> validationLayers = {
};
#endif // DEBUG

#define TEST_ARRAY_SIZE 65535

void iterativeF(std::complex<float> * x, unsigned int N, unsigned int i, unsigned int l) {

  unsigned int n = (N >> l);
  unsigned int m = (N >> (l+1));
  unsigned int mask = (1 << l) -1;
  unsigned int q = (i & mask) * (2 * m);
  unsigned int p = (i >> l);

  double theta0 = 2 * M_PI / N;
  
  std::complex<float> wp = std::complex<float>(cos(p * theta0), - sin(p * theta0));
  std::complex<float> a = x[q + p + 0];
  std::complex<float> b = x[q + p + m];

  x[q + p + 0] = a + b;
  x[q + p + m] = (a - b) * wp;
  
}

void f(int N, int q, std::complex<float> * x) {

  const int m = N/2;
  const double theta0 = 2 * M_PI / N;
  if (N > 1) {
    for (int p = 0; p < m; ++p) {

      const std::complex<float> wp = std::complex<float>(cos(p*theta0), -sin(p*theta0));
      const std::complex<float> a = x[q + p + 0];
      const std::complex<float> b = x[q + p + m];
      x[q + p + 0] = a + b;
      x[q + p + m] = (a - b) * wp;
      
    }
    f(N/2, q + 0, x);
    f(N/2, q + m, x);
  }
  
};

struct ImageInfo {
  uint32_t width;
  uint32_t height;
  uint32_t cellWidth;
  uint32_t cellHeight;
};

int main(int argc, char ** argv) {

  vkutil::VulkanState & state = vkutil::createInstance(validationLayers);
  vkutil::setupDebugMessenger(state, validationLayers.size());
  vkutil::pickPhysicalDevice(state);
  vkutil::createDevice(state);
  vkutil::createAllocator(state);
  vkutil::createCommandPool(state);

  {
    FILE * imageFile = fopen("0056.png", "rb");
    uint32_t width, height, chanelCount;
    uint8_t * imageData = pngLoadImageData(imageFile, &width, &height, &chanelCount);
    fclose(imageFile);

    uint32_t outWidth = (width + 63) / 64;
    uint32_t outHeight = (height + 63) / 64;
  
    GPUImage srcImage(state, width, height, imageData, chanelCount);
    GPUImage floatImage(state, width, height);
    GPUBuffer floatBuffer(state, sizeof(float) * width * height * 4);
    GPUBuffer outputBuffer(state, sizeof(int32_t) * outWidth * outHeight);
  
    ImageToFloatModule imageModule(state);
    imageModule.prepare();
    imageModule.setInputImage(0, &srcImage);
    imageModule.setOutputImage(1, &floatImage);
    imageModule.updateDescriptors();

    ImageToBufferModule imageToBufferModule(state);
    imageToBufferModule.prepare();
    imageToBufferModule.setInputImage(0, &floatImage);
    imageToBufferModule.setOutputBuffer(1, &floatBuffer);
    imageToBufferModule.updateDescriptors();

    ImageInfo imageInfo;
    imageInfo.width = width;
    imageInfo.height = height;
    imageInfo.cellHeight = 64;
    imageInfo.cellWidth = 64;
  
    AutoDetectModule module(state, "src/opencl/image_test.cl.spirv");
    module.prepare();
    module.setInputBuffer(0, &floatBuffer);
    module.setOutputBuffer(1, &outputBuffer);
    module.setPushConstantPointer(&imageInfo);
    module.updateDescriptors();

    VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);

    imageModule.execute(cmdBuffer, 1);
    imageToBufferModule.execute(cmdBuffer, 1);
    module.execute(cmdBuffer, outWidth, outHeight);

    vkutil::endSingleCommand(cmdBuffer, state);

    uint32_t * resData = (uint32_t *) malloc(sizeof(uint32_t) * outWidth * outHeight);
    outputBuffer.getDataFromGPU(resData);

    std::ofstream file("out.ppm");
    file << "P2\n" << outWidth << " " << outHeight << "\n255\n";
    for (size_t y = 0; y < outHeight; ++y) {
      for (size_t x = 0; x < outWidth; ++x) {

	file << resData[y * outWidth + x] << " ";
      }
      file << "\n";
    }
    file.close();
  }

  double doubleDuration;
  double floatDuration;
  
  {

    double * aData = (double *) malloc(sizeof(double) * TEST_ARRAY_SIZE);
    double * bData = (double *) malloc(sizeof(double) * TEST_ARRAY_SIZE);;

    for (size_t i = 0; i < TEST_ARRAY_SIZE; ++i) {

      aData[i] = i;
      bData[i] = TEST_ARRAY_SIZE - i;
      
    }
    
    GPUBuffer aBuffer(state, aData, sizeof(double) * TEST_ARRAY_SIZE);
    GPUBuffer bBuffer(state, bData, sizeof(double) * TEST_ARRAY_SIZE);
    GPUBuffer cBuffer(state, sizeof(double) * TEST_ARRAY_SIZE);

    aBuffer.uploadToGPU();
    bBuffer.uploadToGPU();

    AutoDetectModule module(state, "src/opencl/test.cl.spirv");
    module.prepare();
    module.setInputBuffer(0, &aBuffer);
    module.setInputBuffer(1, &bBuffer);
    module.setOutputBuffer(2, &cBuffer);
    module.updateDescriptors();

    auto beforeTime = std::chrono::high_resolution_clock::now();
    
    VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);
    module.execute(cmdBuffer, TEST_ARRAY_SIZE);
    vkutil::endSingleCommand(cmdBuffer, state);

    auto afterTime = std::chrono::high_resolution_clock::now();

    doubleDuration = std::chrono::duration<double, std::ratio<1, 1000>>(afterTime - beforeTime).count();
    double * cData = (double *) malloc(sizeof(double) * TEST_ARRAY_SIZE);
    cBuffer.getDataFromGPU(cData);

    for (size_t i = 0; i < TEST_ARRAY_SIZE; ++i) {
      if (cData[i] != (aData[i] + bData[i])) {
	throw dbg::trace_exception("Incorrect result!");
      }
    }
    
    free(aData);
    free(bData);
    free(cData);
    
  }
  
  {

    float * aData = (float *) malloc(sizeof(float) * TEST_ARRAY_SIZE);
    float * bData = (float *) malloc(sizeof(float) * TEST_ARRAY_SIZE);

    for (size_t i = 0; i < TEST_ARRAY_SIZE; ++i) {

      aData[i] = i;
      bData[i] = TEST_ARRAY_SIZE - i;
      
    }
    
    GPUBuffer aBuffer(state, aData, sizeof(float) * TEST_ARRAY_SIZE);
    GPUBuffer bBuffer(state, bData, sizeof(float) * TEST_ARRAY_SIZE);
    GPUBuffer cBuffer(state, sizeof(float) * TEST_ARRAY_SIZE);

    aBuffer.uploadToGPU();
    bBuffer.uploadToGPU();

    AutoDetectModule module(state, "src/opencl/fp32.cl.spirv");
    module.prepare();
    module.setInputBuffer(0, &aBuffer);
    module.setInputBuffer(1, &bBuffer);
    module.setOutputBuffer(2, &cBuffer);
    module.updateDescriptors();

    auto beforeTime = std::chrono::high_resolution_clock::now();
    
    VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);
    module.execute(cmdBuffer, TEST_ARRAY_SIZE);
    vkutil::endSingleCommand(cmdBuffer, state);

    auto afterTime = std::chrono::high_resolution_clock::now();

    floatDuration = std::chrono::duration<double, std::ratio<1, 1000>>(afterTime - beforeTime).count();

    float * cData = (float *) malloc(sizeof(float) * TEST_ARRAY_SIZE);
    cBuffer.getDataFromGPU(cData);

    for (size_t i = 0; i < TEST_ARRAY_SIZE; ++i) {
      if (cData[i] != (aData[i] + bData[i])) {
	throw dbg::trace_exception("Incorrect result!");
      }
    }
    
    free(aData);
    free(bData);
    free(cData);
    
  }

  std::cout << "Double: " << doubleDuration << "ms" << std::endl;
  std::cout << "Float: " << floatDuration << "ms" << std::endl;
  
}
