

float2 mul(float2 a, float2 b);
float2 comp_exp(float theta);

float2 mul(float2 a, float2 b) {
  return (float2) (a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

float2 comp_exp(float theta) {

  // e^(-2 * i * PI / p  * k)
  float s,c;
  s = sincos(theta, &c);
  return (float2) (c, s);
  
}

__kernel void fft_radix2(__global float2 * x, int l) {
  
  unsigned int i = get_global_id(0);
  unsigned int N = get_global_size(0) * 2;
  //y[i] = (float2)(-1, -1);
  //y[i+N/2] = (float2)(-1, -1);

  unsigned int n = (N >> l);
  unsigned int m = (N >> (l+1));

  unsigned int mask = (1 << l) - 1;
  unsigned int q = (i & mask) * (2 * m);
  unsigned int p = (i >> l);
  
  float2 a = x[q + p + 0];
  float2 b = x[q + p + m];
  
  if (m == 1) {
    x[q + p + 0] = a + b;
    x[q + p + m] = a - b;
  } else {
    float c, s;
    s = sincos((float)p * (float)M_PI / (float)(n), &c);
    float2 wp = (float2) (c, -s);
    x[q + p + 0] = a + b;
    x[q + p + m] = mul(a - b, wp);
  }

  //x[q+p+0] = (float2)(q+p+0, p);
  //x[q+p+m] = (float2)(q+p+m, p);
  
}
