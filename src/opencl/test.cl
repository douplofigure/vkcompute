

__kernel void vector_add(__global const double * A, __global const double * B, __global double * C) {

  unsigned int i = get_global_id(0);
  C[i] =A[i] + B[i];

}

