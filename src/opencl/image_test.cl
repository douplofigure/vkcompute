

typedef struct {

  unsigned int width;
  unsigned int height;
  unsigned int cellWidth;
  unsigned int cellHeight;
  
} ImageInfo;

typedef struct {

  double2 position;
  double2 facing;
  
} RobotDescription;

__kernel void image_test(__global const float * imageData, ImageInfo info, __global int * counts) {


  unsigned int cellX = get_global_id(0) * info.cellWidth;
  unsigned int cellY = get_global_id(1) * info.cellHeight;

  int count = 0;
  
  for (unsigned int x = cellX; x < info.cellWidth + cellX; ++x) {
    for (unsigned int y = cellY; y < info.cellHeight + cellY; ++y) {

      float val = imageData[(y * info.width + x) * 4 + 1];
      if (val > (float)24.0 / (float)255.0) {
	count++;
      }
      
    }
  }

  counts[get_global_id(1) * get_global_size(0) + get_global_id(0)] = count;
  
}
