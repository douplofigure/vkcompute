#pragma once

#include <stdint.h>

#include <vector>
#include <string>

namespace spirv {
  enum StorageClass {

    STO_UniformConstant = 0,
    STO_Input = 1,
    STO_Uniform = 2,
    STO_Output = 3,
    STO_Workgroup = 4,
    STO_CrossWorkgroup = 5,
    STO_Private = 6,
    STO_Function = 7,
    STO_Generic = 8,
    STO_PushConstant = 9,
    STO_AtomicCounter = 10,
    STO_Image = 11,
    STO_StorageBuffer = 12,
  
  };

  const char * decodeStorageClassName(StorageClass c);

  class SPIRVType {

  public:
    const std::string name;

    virtual ~SPIRVType() {

    }

    virtual size_t getSize() const = 0;
    
  protected:
    SPIRVType(std::string name) : name(name){
    
    };
  
  
  };

  class TypeVoid : public SPIRVType {

  public:
    TypeVoid() : SPIRVType("Void") {
    }

    size_t getSize() const override {
      return 0;
    }
  
  };

  class TypeBool : public SPIRVType {
  public:
    TypeBool() : SPIRVType("bool") {};
    size_t getSize() const override {
      return 1;
    }
  };
  
  class TypeInt : public SPIRVType {
  public:
    TypeInt(uint32_t size, bool sign) : SPIRVType(std::string("int").append(std::to_string(size))) {
      bitCount = size;
      isSigned = sign;
    }
    uint32_t bitCount;
    bool isSigned;

    size_t getSize() const override {
      return bitCount / 8;
    }
    
  };

  struct TypeFloat : public SPIRVType {
  public:
    TypeFloat(uint32_t size) : SPIRVType(std::string("float").append(std::to_string(size))) {
      bitCount = size;
    }
    uint32_t bitCount;

    size_t getSize() const override {
      return bitCount / 8;
    }
    
  };

  class TypeVector : public SPIRVType {
  public:
    TypeVector(const SPIRVType & componentType, uint32_t size) : SPIRVType(std::string("Vector(").append(componentType.name).append(", ").append(std::to_string(size)).append(")")), componentType(componentType) {
      length = size;
    }
    const SPIRVType & componentType;
    uint32_t length;

    size_t getSize() const override {
      return length * componentType.getSize();
    }
    
  };

  class TypePointer : public SPIRVType {
  public:
    TypePointer(const SPIRVType & type, StorageClass storage) : SPIRVType(std::string("Pointer(").append(type.name).append(")")), type(type) {
      storageClass = storage;
    }
    const SPIRVType & type;
    StorageClass storageClass;

    size_t getSize() const override {
      return type.getSize();
    }
  
  };

  enum ImageDim {
    DIM_1D,
    DIM_2D,
    DIM_3D,
    DIM_CUBE,
    DIM_RECT,
    DIM_BUFFER,
    DIM_SUBPASS_DATA,
  };

  enum ImageFormat {
    FORMAT_UNKNOWN,
    RGBA32f,
    RGBA16f,
    R32f,
    RGBA8,
    RGBA8_SNORM,
    RG32f,
    RG16f,
  };

  struct TypeImage : public SPIRVType {
  public:
    TypeImage(const SPIRVType &sampledType, ImageDim dim, uint32_t depth,
	      bool arrayed, bool ms, uint32_t sampled, ImageFormat format)
      : SPIRVType("Image"), sampledType(sampledType) {
      this->dim = dim;
      this->depth = depth;
      this->arrayed = arrayed;
      this->ms = ms;
      this->sampled = sampled;
      this->format = format;
    }

    const SPIRVType & sampledType;
    ImageDim dim;
    uint32_t depth;
    uint32_t sampled;
    ImageFormat format;
    bool arrayed;
    bool ms;

    size_t getSize() const override {
      return 0;
    }
  
  };

  class TypeArray : public SPIRVType {
  public:
    TypeArray(const SPIRVType & elementType, uint32_t length) : SPIRVType("Array"), elementType(elementType) {
      this->length = length;
    };

    const SPIRVType & elementType;
    uint32_t length;

    size_t getSize() const override {
      return length * elementType.getSize();
    }
  
  };

  class TypeRuntimeArray : public SPIRVType {
  public:
    TypeRuntimeArray(const SPIRVType & elementType) : SPIRVType(std::string("RuntimeArray(").append(elementType.name).append(")")), elementType(elementType) {
    
    };
    const SPIRVType & elementType;

    size_t getSize() const override {
      return 0;
    }
    
  };

  std::string getStructTypeName(std::vector<const SPIRVType *> & memberTypes);
  
  class TypeStruct : public SPIRVType {
  public:
    TypeStruct(std::vector<const SPIRVType *> memberTypes) : SPIRVType(getStructTypeName(memberTypes)), memberTypes(memberTypes) {

    }
    const std::vector<const SPIRVType *> memberTypes;

    size_t getSize() const override {
      size_t s = memberTypes[0]->getSize();
      for (size_t i = 1; i < memberTypes.size(); ++i) {
	size_t nSize = memberTypes[i]->getSize();
	size_t align = s % nSize;
	if (align) {
	  s += (nSize - align);
	}

	s += nSize;
	
      }
      return s;
    }
  
  };
}
