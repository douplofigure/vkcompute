#include "spirv/spirv_type.h"

using namespace spirv;

const char * spirv::decodeStorageClassName(StorageClass c)  {

  switch(c) {

  case STO_UniformConstant:
    return "UniformConstant";

  case STO_Input:
    return "Input";

  case STO_Uniform:
    return "Uniform";

  case STO_Output:
    return "Output";

  case STO_Workgroup:
    return "Workgroup";

  case STO_CrossWorkgroup:
    return "CrossWorkgroup";

  case STO_Private:
    return "Private";

  case STO_Function:
    return "Function";

  case STO_Generic:
    return "Generic";

  case STO_PushConstant:
    return "PushConstant";

  case STO_AtomicCounter:
    return "AtomicCounter";

  case STO_Image:
    return "Image";

  case STO_StorageBuffer:
    return "StorageBuffer";
    
  default:
    return "Unknown";
    
  }
  
}

std::string spirv::getStructTypeName(std::vector<const SPIRVType *> &memberTypes) {

  std::string name = "Struct(";

  if (memberTypes.size())
    name.append(memberTypes[0]->name);

  for (size_t i = 1; i < memberTypes.size(); ++i) {
    name.append(", ").append(memberTypes[i]->name);
  }
  
  name.append(")");
  return name;
  
}
