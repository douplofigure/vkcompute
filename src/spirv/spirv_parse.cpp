#include "spirv/spirv_parse.h"

#include <fstream>
#include <iostream>

namespace spirv {

  enum OpCode {

    OpNop = 0,
    OpUndef,
    OpSourceContinued,
    OpSource,
    OpSourceExtension,
    OpName,
    OpMemberName,
    OpString,
    OpLine,

    OpExtension = 10,
    OpExtInstImport,
    OpExtInst,

    OpMemoryModel = 14,
    OpEntryPoint,
    OpExecutionMode,
    OpCapability,

    OpTypeVoid = 19,
    OpTypeBool,
    OpTypeInt,
    OpTypeFloat,
    OpTypeVector,
    OpTypeMatrix,
    OpTypeImage,
    OpTypeSampler,
    OpTypeSampledImage,
    OpTypeArray,
    OpTypeRuntimeArray,
    OpTypeStruct,
    OpTypeOpaque,
    OpTypePointer,
    OpTypeFunction,
    OpTypeEvent,
    OpTypeDeviceEvent,
    OpTypeReserveId,
    OpTypeQueue,
    OpTypePipe,
    OpTypeForwardPointer,

    OpConstantTrue = 41,
    OpConstantFalse,
    OpConstant,
    OpConstantComposite,
    OpConstantSampler,
    OpConstantNull,
    OpSpecConstantTrue,
    OpSpecConstantFalse,
    OpSpecConstant,
    OpSpecConstantComposite,
    OpSpecConstantOp,

    OpFunction = 54,
    OpFunctionParameter,
    OpFunctionEnd,
    OpFunctionCall,

    OpVariable = 59,
    OpImageTexelPointer,
    OpLoad,
    OpCopyMemory,
    OpCopyMemorySized,
    OpAccessChain,
    OpInBoundsAccessChain,
    OpPtrAccessChain,
    OpArrayLength,
    OpGenericPtrMemSemantics,
    OpInBoundsPtrAccessChain,
  

    OpDecorate = 71,
    OpMemberDecorate,
    OpDecorationGroup,
    OpGroupDecorate,
    OpGroupMemberDecorate,

    OpSampledImage = 86,
    OpImageSampleImplicitLod,
    OpImageSampleExplicitLod,
  

    OpNotLine = 317,
  
  };

  TypeStruct * parseStruct(const Module & module, uint32_t * params, uint32_t length);
  Variable createVariable(const SPIRVType * type, StorageClass storage) {

    Variable var;
    var.initId = -1;
    var.storageClass = storage;
    var.type = type;
    return var;
    
  }
  
  Variable createVariable(const SPIRVType * type, StorageClass storage, uint32_t id) {

    Variable var;
    var.initId = id;
    var.storageClass = storage;
    var.type = type;
    return var;
    
  }

  std::shared_ptr<Decoration> createDecoration(uint32_t * params, uint32_t length) {

    std::shared_ptr<Decoration> deco = std::make_shared<Decoration>((DecorationType) params[1]);
    if (length > 3) {
      std::vector<uint32_t> data;
      data.insert(data.end(), params + 2, params + (length - 1));
      deco->data = data;
    }
    return deco;
    
  }
  
}

using namespace spirv;

std::vector<uint32_t> spirv::loadCode(std::string fname) {
    
  std::ifstream stream(fname, std::ios::binary | std::ios::ate);
  size_t size = stream.tellg();
  stream.seekg(0);
  std::cout << "Reading " << size / sizeof(uint32_t) << " words of code from " << fname << "." << std::endl;
  std::vector<uint32_t> data(size / sizeof(uint32_t));
  stream.read((char *) data.data(), size);
  
  return data;
}

TypeStruct * spirv::parseStruct(const Module & module, uint32_t * params, uint32_t length) {

  std::vector<const SPIRVType *> members(length - 2);
  for (uint32_t i = 0; i < length - 2; ++i) {

    const Value & val = module.values[params[i+1]];
    members[i] = val.val.type;
    
  }

  return new TypeStruct(members);
  
}

Module spirv::parse(std::string filename) {

  std::vector<uint32_t> fData = loadCode(filename);

  return parse(fData);
  
}

Module spirv::parse(std::vector<uint32_t> & fData) {

  if (fData[0] != 0x07230203) {
    std::cerr << "File is not valid SPIRV file." << std::endl;
    throw std::exception();
  }

  spirv::Module module;
  module.version = fData[1];
  module.generator = fData[2];
  module.bound = fData[3];
  module.instructionSchema = fData[4];

  module.values = std::vector<Value>(module.bound);
  std::cout << "Bound: " << module.bound << std::endl;

  for (size_t i = 5; i < fData.size();) {

    uint32_t opCode = fData[i] & 0xffff;
    uint32_t length = fData[i] >> 16;

    uint32_t * params = &fData[i+1];

    switch (opCode) {


    case OpExtInstImport:
      std::cout << "Extension import to " << params[0] << " as " << std::string((const char *) &params[1]) << std::endl;;
      break;
      
      // Module-Level Operations
    case OpEntryPoint:
      module.entryPoint = std::string((const char *)(params+2));
      break;

    case OpName:
      module.values[params[0]].name = std::string((const char *) (params+1));
      break;
      
      // Type Declarations
    case OpTypeVoid:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeVoid();
      break;

    case OpTypeBool:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeBool();
      break;

    case OpTypeInt:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeInt(params[1], params[2]);
      break;

    case OpTypeFloat:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeFloat(params[1]);
      break;

    case OpTypeVector:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeVector(*(module.values[params[1]].val.type), params[2]);
      break;

    case OpTypeImage:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeImage(*(module.values[params[1]].val.type), (ImageDim)params[2], params[3], params[4], params[5], params[6], (ImageFormat)params[7]);
      break;

    case OpTypeArray:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeArray(*(module.values[params[1]].val.type), module.values[params[2]].val.i);
      break;

    case OpTypeRuntimeArray:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypeRuntimeArray(*(module.values[params[1]].val.type));
      break;

    case OpTypeStruct:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = parseStruct(module, params, length);
      break;

    case OpTypePointer:
      module.values[params[0]].type = VALUE_TYPE;
      module.values[params[0]].val.type = new TypePointer(*(module.values[params[2]].val.type), (StorageClass) params[1]);
      break;
      
      /// Memory Operations
    case OpVariable:
      module.values[params[1]].type = VALUE_VARIABLE;
      module.variableIds.push_back(params[1]);
      if (length <= 4)
	module.values[params[1]].val.var = createVariable(module.values[params[0]].val.type, (StorageClass) params[2]);
      else
	module.values[params[1]].val.var = createVariable(module.values[params[0]].val.type, (StorageClass) params[2], params[3]);


      // Annotation Operations
    case OpDecorate:
      module.values[params[0]].decorations.push_back(createDecoration(params, length));
      break;
      
    default:
      break;
      
    }

    i += length;
    
  }

  return module;
  
}
