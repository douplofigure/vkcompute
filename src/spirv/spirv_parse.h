#pragma once

#include "spirv/spirv_type.h"

#include <vector>
#include <memory>
#include <string>

namespace spirv {

  enum DecorationType {
    DEC_RelaxedPrecision = 0,
    DEC_SpecId,
    DEC_Block,
    DEC_BufferBlock,
    DEC_RowMajor,
    DEC_ColMajor,
    DEC_ArrayStride,
    DEC_MatrixStride,
    DEC_GLSLShared,
    DEC_GLSLPacked,
    DEC_CPacked,
    DEC_BuiltIn,
    DEC_NoPerspective = 13,
    DEC_Flat,
    DEC_Patch,
    DEC_Centroid,
    DEC_Sample,
    DEC_Invariant,
    DEC_Restrict,
    DEC_Aliased,
    DEC_Volatile,
    DEC_Constant,
    DEC_Coherent,
    DEC_NonWritable,
    DEC_NonReadable,
    DEC_Uniform,
    DEC_UniformId,
    DEC_SaturatedConversion,
    DEC_Stream,
    DEC_Location,
    DEC_Component,
    DEC_Index,
    DEC_Binding,
    DEC_DescriptorSet,
    DEC_Offset,
    DEC_XfbBuffer,
    DEC_XfbStride,
    
  };
  
  enum ValueType {
    VALUE_NONE,
    VALUE_INT,
    VALUE_FLOAT,
    VALUE_STRING,
    VALUE_VARIABLE,
    VALUE_TYPE,
  };

  struct Variable {
    const SPIRVType * type;
    StorageClass storageClass;
    uint32_t initId;
  };

  struct Decoration {

    Decoration(DecorationType type) : type(type) {
    }
    
    const DecorationType type;
    std::vector<uint32_t> data;
    
  };
  
  struct Value {

    ValueType type;
    union {
      
      uint32_t i;
      float f;
      const SPIRVType * type;
      const char * str;
      Variable var;
      
    } val;

    std::vector<std::shared_ptr<Decoration>> decorations;
    std::string name;

    Value() {
      type = VALUE_NONE;
      val = {};
    }

    Value (const SPIRVType & type) {
      val.type = &type;
      this->type = VALUE_TYPE;
    }

    Value (const SPIRVType * type) {
      val.type = type;
      this->type = VALUE_TYPE;
    }

    Value (const char * str) {
      val.str = str;
      this->type = VALUE_STRING;
    }

    Value (Variable var) {
      val.var = var;
      this->type = VALUE_VARIABLE;
    }
    
  };
  
  struct Module {

    uint32_t version;
    uint32_t generator;
    uint32_t bound;
    uint32_t instructionSchema;

    std::string entryPoint;
    
    std::vector<Value> values;
    std::vector<uint32_t> variableIds;

    ~Module() {

      for (Value & val : values) {
	if (val.type == VALUE_TYPE)
	  ;
	  //delete val.val.type;
      }
      
    }
    
  };

  std::vector<uint32_t> loadCode(std::string fname);
  Module parse(std::string filename);
  Module parse(std::vector<uint32_t> & data);
  
};
