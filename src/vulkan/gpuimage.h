#pragma once

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include "vulkan/util.h"
#include "vulkan/gpuresource.h"

class GPUImage : public GPUResource {

public:

  GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height);
  GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height, VkFormat format);
  GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height, uint8_t * data);
  GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height, uint8_t * data, uint32_t channelCount);
  virtual ~GPUImage();

  void * getDescriptorInfo() override;

  uint32_t getWidth() const;
  uint32_t getHeight() const;

  void fetchDataFromGPU(void ** res);
  void cleanUpAfterFetch();

protected:

  const uint32_t width;
  const uint32_t height;
  const vkutil::VulkanState & state;
  VkDeviceSize imageSize;
  VkImage image;
  VkImageView imageView;
  VmaAllocation memory;

  VkBuffer stagingBuffer;
  VmaAllocation stagingBufferMemory;

  VkDescriptorImageInfo descInfo;

private:

  static uint8_t * expandImageWithAlpha(uint32_t width, uint32_t height, uint8_t * data);

  
};
