#include "vulkan/gpuimage.h"

#include <string.h>
#include <iostream>

GPUImage::GPUImage(const vkutil::VulkanState &state, uint32_t width, uint32_t height)
    : GPUImage(state, width, height, VK_FORMAT_R32G32B32A32_SFLOAT) {
}

GPUImage::GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height, VkFormat format) : state(state), width(width), height(height) {

  imageSize = sizeof(float) * width * height * 4;
  if (format == VK_FORMAT_R8G8B8A8_UNORM)
    imageSize = sizeof(uint8_t) * width * height * 4;
  
  vkutil::createStorageImage(state, format, width, height, image, memory);
  VkImageMemoryBarrier barrier2 = {};
  barrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier2.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  barrier2.newLayout = VK_IMAGE_LAYOUT_GENERAL;
  barrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.image = image;
  barrier2.srcAccessMask = 0;
  barrier2.dstAccessMask = 0;
  barrier2.subresourceRange.baseMipLevel = 0;
  barrier2.subresourceRange.levelCount = 1;
  barrier2.subresourceRange.baseArrayLayer = 0;
  barrier2.subresourceRange.layerCount = 1;
  barrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

  VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);
  vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier2);
  vkutil::endSingleCommand(cmdBuffer, state);

  VkImageViewCreateInfo viewCreateInfo = {};
  viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  viewCreateInfo.image = image;
  viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  viewCreateInfo.format = format;
  viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  viewCreateInfo.subresourceRange.baseMipLevel = 0;
  viewCreateInfo.subresourceRange.levelCount = 1;
  viewCreateInfo.subresourceRange.layerCount = 1;
  viewCreateInfo.subresourceRange.baseArrayLayer = 0;

  vkCreateImageView(vkutil::getDevice(state), &viewCreateInfo, nullptr, &imageView);
  
  descInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
  descInfo.imageView = imageView;
  descInfo.sampler = VK_NULL_HANDLE;
  
}

GPUImage::GPUImage(const vkutil::VulkanState &state, uint32_t width,
                   uint32_t height, uint8_t *data)
    : GPUImage(state, width, height, data, 4){};

GPUImage::GPUImage(const vkutil::VulkanState & state, uint32_t width, uint32_t height, uint8_t * inData, uint32_t channelCount) : state(state), width(width), height(height) {

  imageSize = 4 * sizeof(uint8_t) * width * height;

  VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
  uint8_t * data = inData;
  if (channelCount == 3) {
    data = expandImageWithAlpha(width, height, inData);
  }
  
  vkutil::createStagingBuffer(state, imageSize, stagingBuffer, stagingBufferMemory);

  void * dest = vkutil::mapMemory(state, stagingBufferMemory);
  memcpy(dest, data, imageSize);
  vkutil::unmapMemory(state, stagingBufferMemory);
  
  vkutil::createStorageImage(state, format, width, height, image, memory);

  VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);

  VkPipelineStageFlags sourceStage;
  VkPipelineStageFlags destinationStage;
  
  VkImageMemoryBarrier barrier = {};
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.image = image;
  barrier.srcAccessMask = 0;
  barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = 1;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;
  barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  
  sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
  destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

  VkBufferImageCopy region = {};
  region.bufferOffset = 0;
  region.bufferImageHeight = 0;
  region.bufferRowLength = 0;
  
  region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel = 0;
  region.imageSubresource.baseArrayLayer = 0;
  region.imageSubresource.layerCount = 1;
  
  region.imageExtent = {width, height, 1};
  region.imageOffset = {0,0,0};

  vkCmdPipelineBarrier(cmdBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

  vkCmdCopyBufferToImage(cmdBuffer, stagingBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

  VkImageMemoryBarrier barrier2 = {};
  barrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier2.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  barrier2.newLayout = VK_IMAGE_LAYOUT_GENERAL;
  barrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.image = image;
  barrier2.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
  barrier2.dstAccessMask = 0;
  barrier2.subresourceRange.baseMipLevel = 0;
  barrier2.subresourceRange.levelCount = 1;
  barrier2.subresourceRange.baseArrayLayer = 0;
  barrier2.subresourceRange.layerCount = 1;
  barrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

  vkCmdPipelineBarrier(cmdBuffer, destinationStage, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier2);

  vkutil::endSingleCommand(cmdBuffer, state);

  vkutil::destroyBuffer(state, stagingBuffer, stagingBufferMemory);

  if (channelCount == 3) {
    delete [] data;
  }

  VkImageViewCreateInfo viewCreateInfo = {};
  viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  viewCreateInfo.image = image;
  viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
  viewCreateInfo.format = format;
  viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  viewCreateInfo.subresourceRange.baseMipLevel = 0;
  viewCreateInfo.subresourceRange.levelCount = 1;
  viewCreateInfo.subresourceRange.layerCount = 1;
  viewCreateInfo.subresourceRange.baseArrayLayer = 0;

  vkCreateImageView(vkutil::getDevice(state), &viewCreateInfo, nullptr, &imageView);
  
  descInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
  descInfo.imageView = imageView;
  descInfo.sampler = VK_NULL_HANDLE;
  
}

GPUImage::~GPUImage() {

  
  
}

void GPUImage::fetchDataFromGPU(void **res) {

  std::cout << "Image has " << imageSize << " bytes to fetch" << std::endl;
  
  vkutil::createDownloadBuffer(state, imageSize, stagingBuffer, stagingBufferMemory);
  
  VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);

  VkImageMemoryBarrier barrier = {};
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
  barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.image = image;
  barrier.srcAccessMask = 0;
  barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = 1;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;
  barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

  VkBufferImageCopy region = {};
  region.bufferOffset = 0;
  region.bufferImageHeight = 0;
  region.bufferRowLength = 0;
  region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel = 0;
  region.imageSubresource.baseArrayLayer = 0;
  region.imageSubresource.layerCount = 1;
  region.imageExtent = {width, height, 1};
  region.imageOffset = {0,0,0};

  vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier);
  vkCmdCopyImageToBuffer(cmdBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, stagingBuffer, 1, &region);

  VkImageMemoryBarrier barrier2 = {};
  barrier2.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier2.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
  barrier2.newLayout = VK_IMAGE_LAYOUT_GENERAL;
  barrier2.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier2.image = image;
  barrier2.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
  barrier2.dstAccessMask = 0;
  barrier2.subresourceRange.baseMipLevel = 0;
  barrier2.subresourceRange.levelCount = 1;
  barrier2.subresourceRange.baseArrayLayer = 0;
  barrier2.subresourceRange.layerCount = 1;
  barrier2.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

  vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier2);

  vkutil::endSingleCommand(cmdBuffer, state);
  
  *res = malloc(imageSize);
  void * mappedData = vkutil::mapMemory(state, stagingBufferMemory);
  memcpy(*res, mappedData, imageSize);
  vkutil::unmapMemory(state, stagingBufferMemory);
  
  vkutil::destroyBuffer(state, stagingBuffer, stagingBufferMemory);
  
}

void GPUImage::cleanUpAfterFetch() {
}

uint8_t * GPUImage::expandImageWithAlpha(uint32_t width, uint32_t height, uint8_t * inData) {

  uint8_t * outData = new uint8_t[width * height * 4];

  for (size_t i = 0; i < width * height; ++i) {
    outData[i*4] = inData[i*3];
    outData[i*4+1] = inData[i*3+1];
    outData[i*4+2] = inData[i*3+2];
    outData[i*4+3] = 255;
  }

  return outData;
  
}

void * GPUImage::getDescriptorInfo() {

  return &descInfo;
  
}

uint32_t GPUImage::getWidth() const {
  return width;
}

uint32_t GPUImage::getHeight() const {
  return height;
}
