#pragma once

#include "vulkan/computemodule.h"

template <const char ** srcName> class ImageFormatConvertModule : public ComputeModule {

public:
  ImageFormatConvertModule(const vkutil::VulkanState & state) : ComputeModule(state, *srcName) {
    
  }

  std::vector<ComputeModule::IOLayoutInfo> getIOLayout() {
    IOLayoutInfo inputInfo;
    inputInfo.binding = 0;
    inputInfo.count = 1;
    inputInfo.type = IO_IMAGE;

    IOLayoutInfo outputInfo;
    outputInfo.binding = 1;
    outputInfo.count = 1;
    outputInfo.type = IO_IMAGE;
  
    return {inputInfo, outputInfo};
  }

  uint32_t getPushConstantSize() {
    return 0;
  }

  void execute(VkCommandBuffer &buffer, uint32_t xCount, uint32_t yCount=1, uint32_t zCount=1) {
    
    vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
    vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descSets[0], 0, nullptr);
    vkCmdDispatch(buffer, descriptors[0].data.image->getWidth(), descriptors[0].data.image->getHeight(), 1);
    
  }
  
};

class ImageToBufferModule : public ComputeModule {

public:
  ImageToBufferModule(const vkutil::VulkanState & state);

  //void prepare() override;
  uint32_t getPushConstantSize() override;
  std::vector<IOLayoutInfo> getIOLayout() override;
  void execute(VkCommandBuffer &buffer, uint32_t xCount, uint32_t yCount=1, uint32_t zCount=1) override;
  
};

extern const char * rgba8_to_rgb32f;
extern const char * rgba32f_to_rgb8;

typedef ImageFormatConvertModule<&rgba8_to_rgb32f> ImageToFloatModule;
typedef ImageFormatConvertModule<&rgba32f_to_rgb8> ImageToByteModule;
