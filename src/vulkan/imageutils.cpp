#include "vulkan/imageutils.h"

const char * rgba8_to_rgb32f = "src/shaders/rgba8u_to_rgba32f.comp.spirv";
const char * rgba32f_to_rgb8 = "src/shaders/rgba32f_to_rgba8u.comp.spirv";

ImageToBufferModule::ImageToBufferModule(const vkutil::VulkanState & state) : ComputeModule(state, "src/shaders/image_to_buffer.comp.spirv") {

}

/*void ImageToBufferModule::prepare() {

  std::vector<VkDescriptorSetLayoutBinding> layoutBindings;

  for (const IOLayoutInfo & info : getIOLayout()) {
    VkDescriptorSetLayoutBinding binding = {};
    binding.binding = info.binding;
    binding.descriptorCount = info.count;
    binding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    binding.descriptorType = getIOType(info.type);

    layoutBindings.push_back(binding);

    Descriptor desc;
    desc.binding = info.binding;
    desc.count = info.count;
    desc.data = {nullptr};
    desc.type = info.type;
    desc.name = info.name;

    descriptors.push_back(desc);
  }

  VkPushConstantRange constantRange = {};
  constantRange.offset = 0;
  constantRange.size = 2 * sizeof(uint32_t);
  constantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

  descLayout = vkutil::createDescriptorSetLayout(state, layoutBindings);
  pipelineLayout = vkutil::createPipelineLayout(state, descLayout, constantRange);
  pipeline = vkutil::createComputePipeline(state, shaderModule, pipelineLayout, entryName.c_str());
  descPool = vkutil::createDescriptorPool(state, computePoolSize());
  descSets = vkutil::allocateDescriptorSets(state, descPool, descLayout);
  
  }*/

std::vector<ComputeModule::IOLayoutInfo> ImageToBufferModule::getIOLayout() {

  IOLayoutInfo inputInfo;
  inputInfo.binding = 0;
  inputInfo.count = 1;
  inputInfo.type = IO_IMAGE;
  
  IOLayoutInfo outputInfo;
  outputInfo.binding = 1;
  outputInfo.count = 1;
  outputInfo.type = IO_BUFFER;
  
  return {inputInfo, outputInfo};
  
}

uint32_t ImageToBufferModule::getPushConstantSize() {
  return 2 * sizeof(uint32_t);
}

void ImageToBufferModule::execute(VkCommandBuffer &buffer, uint32_t xCount, uint32_t yCount, uint32_t zCount) {

  uint32_t imgSize[2] = {
    descriptors[0].data.image->getWidth(),
    descriptors[0].data.image->getHeight()
  };
  vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
  vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descSets[0], 0, nullptr);
  vkCmdPushConstants(buffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 2*sizeof(uint32_t), imgSize);
  vkCmdDispatch(buffer, imgSize[0], imgSize[1], 1);
  
}
