#pragma once

#include <vulkan/vulkan.h>
#include "vulkan/util.h"
#include "vulkan/gpubuffer.h"
#include "vulkan/gpuimage.h"

class ComputeModule {

public:

  enum IOType {
    IO_NONE,
    IO_BUFFER,
    IO_UNIFORM_BUFFER,
    IO_IMAGE,
  };

  union GPUDataType {
    GPUBuffer * buffer;
    GPUImage * image;
  };
  
  struct Input {

    Input() {
      type = IO_NONE;
      binding = 0;
      count = 1;
    }
    
    IOType type;
    GPUDataType data;
    uint32_t binding;
    uint32_t count;
  };

  struct Output {
    IOType type;
    GPUDataType data;
    uint32_t binding;
    uint32_t count;
  };

  struct Descriptor {
    IOType type;
    GPUDataType data;
    uint32_t binding;
    uint32_t count;
    std::string name;
  };

  struct IOLayoutInfo {
    IOType type;
    uint32_t count;
    uint32_t binding;
    std::string name;
  };

  ComputeModule(const vkutil::VulkanState & state, std::string srcFileName);
  ComputeModule(const vkutil::VulkanState & state, const char * srcFileName);
  virtual ~ComputeModule();

  virtual void prepare();

  virtual std::vector<IOLayoutInfo> getIOLayout() = 0;
  virtual uint32_t getPushConstantSize() = 0;

  std::vector<Descriptor> getDescriptors();
  

  void setInputBuffer(size_t index, GPUBuffer * buffer);
  void setInputImage(size_t index, GPUImage * buffer);

  void setOutputBuffer(size_t index, GPUBuffer * buffer);
  void setOutputImage(size_t index, GPUImage * buffer);

  void updateDescriptors();

  virtual void execute(VkCommandBuffer & cmdBuffer, uint32_t xCount, uint32_t yCount=1, uint32_t zCount=1);

  void setPushConstantPointer(void * ptr);

protected:

  ComputeModule(const vkutil::VulkanState & state, const char * code, size_t size);
  ComputeModule(const vkutil::VulkanState & state);

  std::vector<VkDescriptorPoolSize> computePoolSize();

  std::vector<Descriptor> descriptors;
  
  const vkutil::VulkanState & state;
  
  VkDescriptorSetLayout descLayout;
  VkShaderModule shaderModule;
  VkPipelineLayout pipelineLayout;
  VkPipeline pipeline;

  std::vector<VkDescriptorSet> descSets;
  VkDescriptorPool descPool;
  std::string entryName;

  void * pushConstantValues;

  static VkDescriptorType getIOType(ComputeModule::IOType type);

private:

  
};

class AutoDetectModule : public ComputeModule {

public:
  AutoDetectModule(const vkutil::VulkanState & state, std::string srcFileName);

  std::vector<IOLayoutInfo> getIOLayout() override;
  uint32_t getPushConstantSize() override;

private:
  std::vector<IOLayoutInfo> parsedLayout;
  uint32_t parsedPushConstantSize;

  static std::vector<IOLayoutInfo> extractLayoutFromShaderCode(std::vector<uint32_t> & moduleData, std::string & entryName, uint32_t * pushConstantRange);
  
};

class TestComputeModule : public ComputeModule {

public:
  TestComputeModule(const vkutil::VulkanState & state);
  virtual ~TestComputeModule();

  std::vector<IOLayoutInfo> getIOLayout() override;
  uint32_t getPushConstantSize() override;
  
};

/*class ImageToFloatModule : public ComputeModule {

public:
  ImageToFloatModule(const vkutil::VulkanState & state);

  std::vector<IOLayoutInfo> getInputLayout() override;
  std::vector<IOLayoutInfo> getOutputLayout() override;
  std::vector<IOLayoutInfo> getCombinedIOLayout() override;

  void execute(VkCommandBuffer & buffer, uint32_t xCount, uint32_t yCount=1, uint32_t zCount=1) override;
  
  };*/
