#include "vulkan/gpubuffer.h"

#include "string.h"
#include <iostream>
#include "debug/trace_exception.h"

GPUBuffer::GPUBuffer(const vkutil::VulkanState &state, size_t size) : state(state) {

  vkutil::createBuffer(state, size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer, memory);
  bufferSize = size;
  vkutil::createStagingBuffer(state, bufferSize, stagingBuffer, stagingBufferMemory);

  descInfo.buffer = buffer;
  descInfo.offset = 0;
  descInfo.range = bufferSize;

}

GPUBuffer::GPUBuffer(const vkutil::VulkanState &state, std::vector<uint8_t> data)
  : GPUBuffer(state, data.data(), data.size()) {};

GPUBuffer::GPUBuffer(const vkutil::VulkanState &state, void *data, size_t size)
    : state(state) {

  vkutil::createBuffer(state, size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer, memory);
  bufferSize = size;
  vkutil::createStagingBuffer(state, bufferSize, stagingBuffer, stagingBufferMemory, data, size);

  descInfo.buffer = buffer;
  descInfo.offset = 0;
  descInfo.range = bufferSize;
  
}

GPUBuffer::~GPUBuffer() {

  vkutil::destroyBuffer(state, buffer, memory);
  vkutil::destroyBuffer(state, stagingBuffer, stagingBufferMemory);
  
}

void GPUBuffer::uploadToGPU() {

  vkutil::copyBuffer(state, stagingBuffer, buffer, bufferSize);
  
}

void GPUBuffer::getDataFromGPU(void *dest) {

  std::cout << "Fetching " << bufferSize << " bytes from GPU" << std::endl;
  
  vkutil::copyBuffer(state, buffer, stagingBuffer, bufferSize);
  void * loc = vkutil::mapMemory(state, stagingBufferMemory);
  memcpy(dest, loc, bufferSize);
  vkutil::unmapMemory(state, stagingBufferMemory);
  
}

void * GPUBuffer::mapToMemory() {
  return vkutil::mapMemory(state, stagingBufferMemory);
}

void GPUBuffer::unmapFromMemory() {
  vkutil::unmapMemory(state, stagingBufferMemory);
}

VkBuffer GPUBuffer::getBuffer() const {
  return buffer;
}

VkDeviceSize GPUBuffer::getSize() const {
  return bufferSize;
}

void * GPUBuffer::getDescriptorInfo() {
  return &descInfo;
}

void GPUBuffer::copyTo(VkCommandBuffer &cmdBuffer, GPUBuffer &dest) {

  if (bufferSize > dest.bufferSize)
    throw dbg::trace_exception("Buffer copy has not enough space in destination.");
  
  VkBufferCopy region = {};
  region.srcOffset = 0;
  region.dstOffset = 0;
  region.size = bufferSize;

  vkCmdCopyBuffer(cmdBuffer, this->buffer, dest.buffer, 1, &region);
}

void GPUBuffer::copyTo(GPUBuffer &dest) {

  VkCommandBuffer cmdBuffer = vkutil::beginSingleCommand(state);

  copyTo(cmdBuffer, dest);

  vkutil::endSingleCommand(cmdBuffer, state);
  
}
