#pragma once


class GPUResource {

public:

  virtual void * getDescriptorInfo() = 0;
  
};
