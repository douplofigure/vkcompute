#include "vulkan/computeprogram.h"

#include <vulkan/vulkan.h>
#include "vulkan/util.h"

ComputeProgram::ComputeProgram(const vkutil::VulkanState & state, std::string srcFilename) : state(state) {

  FILE * file = fopen(srcFilename.c_str(), "r");
  fseek(file, 0, SEEK_END);
  size_t size = ftell(file);
  fseek(file, 0, SEEK_SET);

  char * code = (char *) malloc(size);
  fread(code, size, 1, file);
  fclose(file);
  
  shaderModule = vkutil::createShaderModule(state, code, size);
  descLayout = vkutil::createDescriptorSetLayout(state);
  pipelineLayout = vkutil::createPipelineLayout(state, descLayout);
  pipeline = vkutil::createComputePipeline(state, shaderModule, pipelineLayout);
  descPool = vkutil::createDescriptorPool(state);
  descSets = vkutil::allocateDescriptorSets(state, descPool, descLayout);
  
}

ComputeProgram::~ComputeProgram() {
  VkDevice device = vkutil::getDevice(state);
  
  vkDestroyDescriptorPool(device, descPool, nullptr);
  vkDestroyPipeline(device, pipeline, nullptr);
  vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
  vkDestroyDescriptorSetLayout(device, descLayout, nullptr);
  vkDestroyShaderModule(device, shaderModule, nullptr);
  
}

void ComputeProgram::setInputBuffer(const GPUBuffer &buffer) {

  inputBuffer = &buffer;
  
}

void ComputeProgram::setOutputBuffer(const GPUBuffer &buffer) {
  outputBuffer = &buffer;
}

void ComputeProgram::updateDescriptors() {

  VkDescriptorBufferInfo inputInfo = {};
  inputInfo.buffer = inputBuffer->getBuffer();
  inputInfo.offset = 0;
  inputInfo.range = inputBuffer->getSize();

  VkWriteDescriptorSet inputWrite = {};
  inputWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
  inputWrite.descriptorCount = 1;
  inputWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  inputWrite.dstArrayElement = 0;
  inputWrite.pBufferInfo = &inputInfo;
  inputWrite.dstBinding = 0;
  inputWrite.dstSet = descSets[0];
		   
  VkDescriptorBufferInfo outputInfo = {};
  outputInfo.buffer = outputBuffer->getBuffer();
  outputInfo.offset = 0;
  outputInfo.range = outputBuffer->getSize();

  VkWriteDescriptorSet outputWrite = {};
  outputWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
  outputWrite.descriptorCount = 1;
  outputWrite.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  outputWrite.dstArrayElement = 0;
  outputWrite.pBufferInfo = &outputInfo;
  outputWrite.dstBinding = 1;
  outputWrite.dstSet = descSets[0];

  VkWriteDescriptorSet writes[2] = {inputWrite, outputWrite};

  vkUpdateDescriptorSets(vkutil::getDevice(state), 2, writes, 0, nullptr);
  
}

void ComputeProgram::execute(uint32_t xCount, uint32_t yCount, uint32_t zCount) {

  VkCommandBuffer commandBuffer = vkutil::beginSingleCommand(state);

  vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
  vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descSets[0], 0, nullptr);
  vkCmdDispatch(commandBuffer, xCount, yCount, zCount);
  
  vkutil::endSingleCommand(commandBuffer, state);

  //vkDeviceWaitIdle(vkutil::getDevice(state));
  
}
