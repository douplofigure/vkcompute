#pragma once

#include <vulkan/vulkan.h>

#include "vulkan/util.h"
#include "vulkan/gpubuffer.h"

class ComputeProgram {

public:
  ComputeProgram(const vkutil::VulkanState & state, std::string srcFileName);

  virtual ~ComputeProgram();

  void setInputBuffer(const GPUBuffer & buffer);
  void setOutputBuffer(const GPUBuffer & buffer);

  void updateDescriptors();

  void execute(uint32_t xCount, uint32_t yCount=1, uint32_t zCount=1);

private:

  const vkutil::VulkanState & state;
  
  VkDescriptorSetLayout descLayout;
  VkShaderModule shaderModule;
  VkPipelineLayout pipelineLayout;
  VkPipeline pipeline;

  std::vector<VkDescriptorSet> descSets;
  VkDescriptorPool descPool;

  const GPUBuffer * inputBuffer;
  const GPUBuffer * outputBuffer;
  
  
};
