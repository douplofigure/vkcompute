#include "vulkan/util.h"

#include <vulkan/vulkan.h>
#include <string.h>
#include <string>

#include "debug/trace_exception.h"
#include <vk_mem_alloc.h>

#include "vulkan/vk_trace_exception.h"

#include "debug/logger.h"
#include <limits>

#define VK_CHECK(exp, msg) {if (VkResult res = exp) throw vkutil::vk_trace_exception(msg, res);}

namespace vkutil {

  struct VulkanState {

    VulkanState() {
    }

    VkInstance instance;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    uint32_t computeQueueFamilyIndex;
    VkQueue computeQueue;
    VmaAllocator vmaAllocator;
    VkCommandPool computeCommandPool;
  };
}

VkDebugUtilsMessengerEXT debugMessenger;

using namespace vkutil;

bool checkValidationLayerSupport(std::vector<const char *> validationLayers) {

  uint32_t layerCount;
  vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
  std::vector<VkLayerProperties> properties(layerCount);
  vkEnumerateInstanceLayerProperties(&layerCount, properties.data());

  for (const char * name : validationLayers) {

    bool layerFound = false;
    for (auto layerProperties : properties) {

      if (!strcmp(name, layerProperties.layerName)) {
	layerFound = true;
	break;
      }

    }

    if (!layerFound) {
      throw dbg::trace_exception(std::string("Missing layer: ").append(name));
      return false;
    }

  }

  return true;

}

VulkanState & vkutil::createInstance(std::vector<const char *> validationLayers) {

  if (validationLayers.size() && !checkValidationLayerSupport(validationLayers))
    throw dbg::trace_exception("Validation layers not found");

  VkApplicationInfo appInfo = {};
  appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pApplicationName = "VulkanCompute";
  appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
  appInfo.pEngineName = "VulkanCompute";
  appInfo.engineVersion = VK_MAKE_VERSION(1,0,0);
  appInfo.apiVersion = VK_API_VERSION_1_2;

  VkInstanceCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  createInfo.pApplicationInfo = &appInfo;

  std::vector<const char *> requiredExtensions;
  requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

  createInfo.enabledExtensionCount = requiredExtensions.size();
  createInfo.ppEnabledExtensionNames = requiredExtensions.data();
  createInfo.enabledLayerCount = validationLayers.size();
  if (validationLayers.size())
    createInfo.ppEnabledLayerNames = validationLayers.data();
  else
    createInfo.ppEnabledLayerNames = nullptr;

  VkInstance instance;
  /*if (VkResult res = vkCreateInstance(&createInfo, nullptr, &instance))
    throw vkutil::vk_trace_exception("Error creating instance", res);*/
  VK_CHECK(vkCreateInstance(&createInfo, nullptr, &instance), "Error creating instance");

  VulkanState * state = new VulkanState();
  state->instance = instance;

  return *state;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {

  if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
    throw dbg::trace_exception(pCallbackData->pMessage);
  } else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
#ifdef THROW_ON_WARN
    throw dbg::trace_exception(pCallbackData->pMessage);
#else
    lerr << "WARNING : " << pCallbackData->pMessage << std::endl;
#endif
  } else {
    lout << "DEBUG   : " << pCallbackData->pMessage << std::endl;
  }
  //exit(1);
  return VK_FALSE;

}

void vkutil::setupDebugMessenger(const VulkanState & state, bool validationLayers) {

  if (!validationLayers) return;

  VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
  createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
  createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
  createInfo.pfnUserCallback = debugCallback;

  /*if (vkCreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
    throw dbg::trace_exception("failed to set up debug messenger!");
    }*/

  //VK_CHECK(vkCreateDebugUtilsMessengerEXT(state.instance, &createInfo, nullptr, &debugMessenger), "Failed to setup debug messenger");
  
}

bool isDeviceSuitable(VkPhysicalDevice device, VulkanState & state) {

  VkPhysicalDeviceFeatures supportedFeatures;
  vkGetPhysicalDeviceFeatures(device, &supportedFeatures);

  if (!supportedFeatures.shaderFloat64) {
    std::cerr << "Device has no support for float64 operations" << std::endl;
    return false;
  }

  bool hasComputeQueue = false;
  uint32_t familyCount = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(device, &familyCount, nullptr);
  std::vector<VkQueueFamilyProperties> families(familyCount);
  vkGetPhysicalDeviceQueueFamilyProperties(device, &familyCount, families.data());

  uint32_t computeIndex = 0;
  uint32_t i = 0;
  
  for (VkQueueFamilyProperties p : families) {
    if (p.queueFlags & VK_QUEUE_COMPUTE_BIT) {
      hasComputeQueue = true;
      state.computeQueueFamilyIndex = i;
    }
    ++i;
  }

  return hasComputeQueue;
  
};

void vkutil::pickPhysicalDevice(VulkanState & state) {

  uint32_t deviceCount;
  vkEnumeratePhysicalDevices(state.instance, &deviceCount, nullptr);
  if (!deviceCount) throw dbg::trace_exception("No GPU found");

  std::vector<VkPhysicalDevice> devices(deviceCount);
  vkEnumeratePhysicalDevices(state.instance, &deviceCount, devices.data());

  VkPhysicalDevice device = VK_NULL_HANDLE;

  for (VkPhysicalDevice d : devices) {

    if (isDeviceSuitable(d, state)) {
      device = d;
      break;
    }

  }

  if (device == VK_NULL_HANDLE)
    throw dbg::trace_exception("unable to find physical device");

  state.physicalDevice = device;

}

void vkutil::createDevice(VulkanState &state) {

  uint32_t count;
  vkEnumerateDeviceExtensionProperties(state.physicalDevice, nullptr, &count, nullptr);
  std::vector<VkExtensionProperties> available(count);
  vkEnumerateDeviceExtensionProperties(state.physicalDevice, nullptr, &count, available.data()); //populate buffer

  /*for (auto & extension : available) {

    std::cout << extension.extensionName << " : " << extension.specVersion << std::endl;
    
    }*/
  
  float queuePriorities[1] = {1};

  VkDeviceQueueCreateInfo computeQueueCreateInfo = {};
  computeQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  computeQueueCreateInfo.queueFamilyIndex = state.computeQueueFamilyIndex;
  computeQueueCreateInfo.queueCount = 1;
  computeQueueCreateInfo.pQueuePriorities = queuePriorities;

  std::vector<const char *> extensions = {
    "VK_KHR_storage_buffer_storage_class",
    "VK_KHR_shader_non_semantic_info",
    "VK_KHR_variable_pointers"
  };
  
  VkDeviceCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  createInfo.queueCreateInfoCount = 1;
  createInfo.pQueueCreateInfos = &computeQueueCreateInfo;
  createInfo.enabledExtensionCount = extensions.size();
  createInfo.ppEnabledExtensionNames = extensions.data();

  VkPhysicalDeviceFeatures features = {};
  features.shaderFloat64 = 1;

  createInfo.pEnabledFeatures = &features;

  VK_CHECK(vkCreateDevice(state.physicalDevice, &createInfo, nullptr, &state.device), "Error creating device");

  vkGetDeviceQueue(state.device, state.computeQueueFamilyIndex, 0, &state.computeQueue);
}

void vkutil::createAllocator(VulkanState &state) {

  VmaAllocatorCreateInfo createInfo = {};
  createInfo.device = state.device;
  createInfo.physicalDevice = state.physicalDevice;
  createInfo.instance = state.instance;

  vmaCreateAllocator(&createInfo, &state.vmaAllocator);
}

void vkutil::createCommandPool(VulkanState &state) {

  VkCommandPool pool;
  
  VkCommandPoolCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  createInfo.queueFamilyIndex = state.computeQueueFamilyIndex;
  createInfo.pNext = nullptr;
  createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

  VK_CHECK(vkCreateCommandPool(state.device, &createInfo, nullptr, &pool), "Unable to create command pool");
  state.computeCommandPool = pool;
  
}

void vkutil::createBuffer(const VulkanState & state, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer & buffer, VmaAllocation & memory) {

    //VkDevice device = VulkanHelper::getDevice();

    if (properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        throw dbg::trace_exception("Bad buffer alloc");

    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo = {};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    vmaCreateBuffer(state.vmaAllocator, &bufferInfo, &allocInfo, &buffer, &memory, nullptr);

}

void vkutil::destroyBuffer(const VulkanState &state, VkBuffer &buffer, VmaAllocation &memory) {

  vmaDestroyBuffer(state.vmaAllocator, buffer, memory);
  
}

void vkutil::createStagingBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory) {

  VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  stBufferCreateInfo.size = bufferSize;
  stBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
  stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  
  VmaAllocationCreateInfo stAllocCreateInfo = {};
  stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
  stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  
  VmaAllocationInfo stagingBufferAllocInfo = {};
  
  vmaCreateBuffer(state.vmaAllocator, &stBufferCreateInfo, &stAllocCreateInfo, &stagingBuffer, &stagingBufferMemory, &stagingBufferAllocInfo);

  
}

void vkutil::createStagingBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory, void * data, size_t size) {
  
  VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  stBufferCreateInfo.size = bufferSize;
  stBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
  stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  
  VmaAllocationCreateInfo stAllocCreateInfo = {};
  stAllocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
  stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  
  VmaAllocationInfo stagingBufferAllocInfo = {};
  
  vmaCreateBuffer(state.vmaAllocator, &stBufferCreateInfo, &stAllocCreateInfo, &stagingBuffer, &stagingBufferMemory, &stagingBufferAllocInfo);

  memcpy(stagingBufferAllocInfo.pMappedData, data, size);
  //vmaUnmapMemory(state.vmaAllocator, stagingBufferMemory);
  
}

void vkutil::createDownloadBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory) {

  VkBufferCreateInfo stBufferCreateInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
  stBufferCreateInfo.size = bufferSize;
  stBufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
  stBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  
  VmaAllocationCreateInfo stAllocCreateInfo = {};
  stAllocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;
  stAllocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
  
  VmaAllocationInfo stagingBufferAllocInfo = {};
  
  vmaCreateBuffer(state.vmaAllocator, &stBufferCreateInfo, &stAllocCreateInfo, &stagingBuffer, &stagingBufferMemory, &stagingBufferAllocInfo);

  
}

VkCommandBuffer vkutil::beginSingleCommand(const VulkanState & state) {

  VkCommandBufferAllocateInfo allocInfo = {};
  allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocInfo.commandPool = state.computeCommandPool;
  allocInfo.commandBufferCount = 1;

  VkCommandBuffer commandBuffer;
  vkAllocateCommandBuffers(state.device, &allocInfo, &commandBuffer);

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  vkBeginCommandBuffer(commandBuffer, &beginInfo);

  return commandBuffer;

}

void vkutil::endSingleCommand(VkCommandBuffer & commandBuffer, const VulkanState & state) {

  VkFenceCreateInfo fenceInfo = {};
  fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceInfo.pNext = nullptr;
  fenceInfo.flags = 0;

  VkFence fence;
  vkCreateFence(state.device, &fenceInfo, nullptr, &fence);
  vkEndCommandBuffer(commandBuffer);

  VkSubmitInfo submitInfo = {};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;


  vkQueueSubmit(state.computeQueue, 1, &submitInfo, fence);
  vkWaitForFences(state.device, 1, &fence, true, std::numeric_limits<uint64_t>::max());

  vkDestroyFence(state.device, fence, nullptr);
  vkFreeCommandBuffers(state.device, state.computeCommandPool, 1, &commandBuffer);

}

void vkutil::copyBuffer(const VulkanState &state, VkBuffer source, VkBuffer dest, size_t size) {

  VkCommandBuffer commandBuffer = beginSingleCommand(state);

  VkBufferCopy copyRegion = {};
  copyRegion.srcOffset = 0;
  copyRegion.dstOffset = 0;
  copyRegion.size = size;

  vkCmdCopyBuffer(commandBuffer, source, dest, 1, &copyRegion);
  endSingleCommand(commandBuffer, state);
  
}

void *vkutil::mapMemory(const VulkanState &state, VmaAllocation &memory) {

  void *loc;
  vmaMapMemory(state.vmaAllocator, memory, &loc);
  return loc;
}

void vkutil::unmapMemory(const VulkanState &state, VmaAllocation &memory) {

  vmaUnmapMemory(state.vmaAllocator, memory);
  
}

VkShaderModule vkutil::createShaderModule(const VulkanState &state, std::string srcCode){

  VkShaderModuleCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  createInfo.codeSize = srcCode.size();
  createInfo.pCode = (uint32_t*) srcCode.c_str();

  VkShaderModule module;
  VK_CHECK(vkCreateShaderModule(state.device, &createInfo, nullptr, &module), "Error creating shader module");

  return module;

}

VkShaderModule vkutil::createShaderModule(const VulkanState &state, const char * srcCode, size_t srcSize){

  VkShaderModuleCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  createInfo.codeSize = srcSize;
  createInfo.pCode = (uint32_t*) srcCode;

  VkShaderModule module;
  VK_CHECK(vkCreateShaderModule(state.device, &createInfo, nullptr, &module), "Error creating shader module");

  return module;

}

VkDescriptorSetLayout vkutil::createDescriptorSetLayout(const VulkanState &state) {

  VkDescriptorSetLayoutBinding descLayoutBindings[2] = {
    {0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_COMPUTE_BIT, 0},
    {1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_COMPUTE_BIT, 0}
  };

  VkDescriptorSetLayoutCreateInfo descLayoutCreateInfo = {
    VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
  0, 0, 2, descLayoutBindings
  };

  VkDescriptorSetLayout descLayout;
  VK_CHECK(vkCreateDescriptorSetLayout(state.device, &descLayoutCreateInfo, nullptr, &descLayout), "Error creating descriptor set layout.");

  return descLayout;
}

VkDescriptorSetLayout vkutil::createDescriptorSetLayout(const VulkanState &state, std::vector<VkDescriptorSetLayoutBinding> bindings) {

  VkDescriptorSetLayoutCreateInfo descLayoutCreateInfo = {
    VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
    0, 0, (uint32_t) bindings.size(), bindings.data()
  };

  VkDescriptorSetLayout descLayout;
  VK_CHECK(vkCreateDescriptorSetLayout(state.device, &descLayoutCreateInfo, nullptr, &descLayout), "Error creating descriptor set layout.");

  return descLayout;
}

VkPipelineLayout vkutil::createPipelineLayout(const VulkanState &state, VkDescriptorSetLayout & descLayout) {

  VkPipelineLayoutCreateInfo layoutCreateInfo = {};
  layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  layoutCreateInfo.setLayoutCount = 1;
  layoutCreateInfo.pSetLayouts = &descLayout;

  VkPipelineLayout layout;
  VK_CHECK(vkCreatePipelineLayout(state.device, &layoutCreateInfo, nullptr, &layout), "Error creating pipeline layout");
  return layout;
  
}

VkPipelineLayout vkutil::createPipelineLayout(const VulkanState &state, VkDescriptorSetLayout & descLayout, VkPushConstantRange & constantRange) {

  VkPipelineLayoutCreateInfo layoutCreateInfo = {};
  layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  layoutCreateInfo.setLayoutCount = 1;
  layoutCreateInfo.pSetLayouts = &descLayout;
  layoutCreateInfo.pushConstantRangeCount = 1;
  layoutCreateInfo.pPushConstantRanges = &constantRange;

  VkPipelineLayout layout;
  VK_CHECK(vkCreatePipelineLayout(state.device, &layoutCreateInfo, nullptr, &layout), "Error creating pipeline layout");
  return layout;
  
}

VkPipeline vkutil::createComputePipeline(const VulkanState & state, VkShaderModule shaderModule, VkPipelineLayout layout) {

  VkComputePipelineCreateInfo createInfo = {
    VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
    0, 0,
    {
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      0, 0, VK_SHADER_STAGE_COMPUTE_BIT, shaderModule, "main", 0
    },
    layout, 0, 0
  };

  VkPipeline pipeline;
  VK_CHECK(vkCreateComputePipelines(state.device, nullptr, 1, &createInfo, nullptr, &pipeline), "Error creating pipeline");

  return pipeline;
  
}

VkPipeline vkutil::createComputePipeline(const VulkanState & state, VkShaderModule shaderModule, VkPipelineLayout layout, const char * entryName) {

  VkComputePipelineCreateInfo createInfo = {
    VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
    0, 0,
    {
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
      0, 0, VK_SHADER_STAGE_COMPUTE_BIT, shaderModule, entryName, 0
    },
    layout, 0, 0
  };

  VkPipeline pipeline;
  VK_CHECK(vkCreateComputePipelines(state.device, nullptr, 1, &createInfo, nullptr, &pipeline), "Error creating pipeline");

  return pipeline;
  
}

VkDevice vkutil::getDevice(const VulkanState &state) {
  return state.device;
}

VkDescriptorPool vkutil::createDescriptorPool(const VulkanState &state) {

  VkDescriptorPoolSize poolSize = {};
  poolSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  poolSize.descriptorCount = 2;

  VkDescriptorPoolCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  createInfo.pPoolSizes = &poolSize;
  createInfo.poolSizeCount = 1;
  createInfo.maxSets = 1;

  VkDescriptorPool pool;
  VK_CHECK(vkCreateDescriptorPool(state.device, &createInfo, nullptr, &pool), "Error creating descriptor pool");

  return pool;
  
}

VkDescriptorPool vkutil::createDescriptorPool(const VulkanState &state, std::vector<VkDescriptorPoolSize> sizes) {

  VkDescriptorPoolCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  createInfo.pPoolSizes = sizes.data();
  createInfo.poolSizeCount = sizes.size();
  createInfo.maxSets = 1;

  VkDescriptorPool pool;
  VK_CHECK(vkCreateDescriptorPool(state.device, &createInfo, nullptr, &pool), "Error creating descriptor pool");

  return pool;
  
}

std::vector<VkDescriptorSet> vkutil::allocateDescriptorSets(const VulkanState &state, VkDescriptorPool pool, VkDescriptorSetLayout layout) {

  VkDescriptorSetLayout layouts[1] = {layout};

  VkDescriptorSetAllocateInfo allocInfo = {};
  allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  allocInfo.descriptorSetCount = 1;
  allocInfo.descriptorPool = pool;
  allocInfo.pSetLayouts = layouts;

  std::vector<VkDescriptorSet> sets(1);
  VK_CHECK(vkAllocateDescriptorSets(state.device, &allocInfo, sets.data()), "Error allocating descriptor sets");
  return sets;
}

void vkutil::createStorageImage(const VulkanState &state, VkFormat format, uint32_t width, uint32_t height, VkImage &image, VmaAllocation &memory) {

  VkImageCreateInfo createInfo = {};
  createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  createInfo.imageType = VK_IMAGE_TYPE_2D;
  createInfo.extent.width = width;
  createInfo.extent.height = height;
  createInfo.extent.depth = 1;
  createInfo.mipLevels = 1;
  createInfo.arrayLayers = 1;
  createInfo.format = format;
  createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
  createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  createInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
  createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  createInfo.flags = 0;
  createInfo.pNext = nullptr;
  createInfo.queueFamilyIndexCount = 0;
  createInfo.pQueueFamilyIndices = nullptr;

  VmaAllocationCreateInfo allocInfo = {};
  allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
  allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

  VmaAllocationInfo resInfo;

  vmaCreateImage(state.vmaAllocator, &createInfo, &allocInfo, &image, &memory, &resInfo);
  //VK_CHECK(, "Failed to create image");
  
}

VkImageView vkutil::createImageView(const VkDevice & device, const VkImage & image, VkFormat format, VkImageAspectFlags aspect, int mipLevels, VkImageViewType viewType, int layerCount) {

  VkImageViewCreateInfo viewInfo = {};
  viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  viewInfo.image = image;
  viewInfo.viewType = viewType;
  viewInfo.format = format;

  viewInfo.subresourceRange.aspectMask = aspect;
  viewInfo.subresourceRange.baseMipLevel = 0;
  viewInfo.subresourceRange.levelCount = mipLevels;
  viewInfo.subresourceRange.baseArrayLayer = 0;
  viewInfo.subresourceRange.layerCount = layerCount;

  VkImageView imageView;
  if (VkResult r = vkCreateImageView(device, &viewInfo, nullptr, &imageView))
    throw vkutil::vk_trace_exception("Unable to create image view", r);

  return imageView;

}
