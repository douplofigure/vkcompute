#pragma once

#include "stdlib.h"
#include <vector>
#include <string>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

namespace vkutil {

  struct VulkanState;

  VkDevice getDevice(const VulkanState & state);
  
  VulkanState & createInstance(std::vector<const char *> validationLayers);
  void setupDebugMessenger(const VulkanState & instance, bool validationLayers);
  void pickPhysicalDevice(VulkanState & state);
  void createDevice(VulkanState & state);
  void createAllocator(VulkanState & state);
  void createCommandPool(VulkanState & state);

  void createBuffer(const VulkanState & state, VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer & buffer, VmaAllocation & memory);
  void destroyBuffer(const VulkanState & state, VkBuffer & buffer, VmaAllocation & memory);
  void createStagingBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory);
  void createStagingBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory, void * data, size_t size);
  void createDownloadBuffer(const VulkanState & state, VkDeviceSize bufferSize, VkBuffer & stagingBuffer, VmaAllocation & stagingBufferMemory);

  void * mapMemory(const VulkanState & state, VmaAllocation & memory);
  void unmapMemory(const VulkanState & state, VmaAllocation & memory);

  VkCommandBuffer beginSingleCommand(const VulkanState & state);
  void endSingleCommand(VkCommandBuffer & commandBuffer, const VulkanState & state);
  
  void copyBuffer(const VulkanState & state, VkBuffer source, VkBuffer dest, size_t size);

  VkShaderModule createShaderModule(const VulkanState &state, std::string srcCode);
  VkShaderModule createShaderModule(const VulkanState &state, const char * srcCode, size_t srcSize);
  VkDescriptorSetLayout createDescriptorSetLayout(const VulkanState & state);
  VkDescriptorSetLayout createDescriptorSetLayout(const VulkanState &state, std::vector<VkDescriptorSetLayoutBinding> bindings);
  VkPipelineLayout createPipelineLayout(const VulkanState & state, VkDescriptorSetLayout & descLayout);
  VkPipelineLayout createPipelineLayout(const VulkanState &state, VkDescriptorSetLayout & descLayout, VkPushConstantRange & constantRange);
  VkPipeline createComputePipeline(const VulkanState & state, VkShaderModule shaderModule, VkPipelineLayout layout);
  VkPipeline createComputePipeline(const VulkanState & state, VkShaderModule shaderModule, VkPipelineLayout layout, const char * entryName);
  VkDescriptorPool createDescriptorPool(const VulkanState &state);
  VkDescriptorPool createDescriptorPool(const VulkanState &state, std::vector<VkDescriptorPoolSize> sizes);

  std::vector<VkDescriptorSet> allocateDescriptorSets(const VulkanState & state, VkDescriptorPool pool, VkDescriptorSetLayout layout);

  void createStorageImage(const VulkanState & state, VkFormat format, uint32_t width, uint32_t height, VkImage & image, VmaAllocation & memory);
  VkImageView createImageView(const VkDevice & device, const VkImage & image, VkFormat format, VkImageAspectFlags aspect, int mipLevels, VkImageViewType viewType, int layerCount);
  
}
