#include "vulkan/computemodule.h"

#include "debug/trace_exception.h"

#include <spirv-tools/libspirv.hpp>

#include <iostream>
#include <sstream>
#include <map>

#include "spirv/spirv_parse.h"

ComputeModule::ComputeModule(const vkutil::VulkanState & state, std::string srcFileName) : ComputeModule(state, srcFileName.c_str()) {
  
}

ComputeModule::ComputeModule(const vkutil::VulkanState & state, const char * srcFileName) : state(state), entryName("main") {

  
  std::vector<uint32_t> moduleData = spirv::loadCode(std::string(srcFileName));
  shaderModule = vkutil::createShaderModule(state, (const char *) moduleData.data(), moduleData.size() * sizeof(uint32_t));
  
}

ComputeModule::ComputeModule(const vkutil::VulkanState & state, const char * code, size_t size) : state(state) {

  shaderModule = vkutil::createShaderModule(state, code, size);
  
}

ComputeModule::ComputeModule(const vkutil::VulkanState & state) : state(state), entryName("main") {

}

ComputeModule::~ComputeModule() {

  VkDevice device = vkutil::getDevice(state);
  
  vkDestroyDescriptorPool(device, descPool, nullptr);
  vkDestroyPipeline(device, pipeline, nullptr);
  vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
  vkDestroyDescriptorSetLayout(device, descLayout, nullptr);
  vkDestroyShaderModule(device, shaderModule, nullptr);
  
}

VkDescriptorType ComputeModule::getIOType(ComputeModule::IOType type) {

  switch (type) {
  case ComputeModule::IO_IMAGE:
    return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
  case ComputeModule::IO_BUFFER:
    return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
  case ComputeModule::IO_UNIFORM_BUFFER:
    return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
  default:
    return VK_DESCRIPTOR_TYPE_MAX_ENUM;  
  }
  
}

void ComputeModule::setInputBuffer(size_t index, GPUBuffer *buffer) {

  if (index >= descriptors.size())
    throw dbg::trace_exception("Input not in range");

  if (descriptors[index].type != IO_BUFFER && descriptors[index].type != IO_UNIFORM_BUFFER)
    throw dbg::trace_exception("Wrong input type 'Buffer'");

  descriptors[index].data.buffer = buffer;
  
}

void ComputeModule::setInputImage(size_t index, GPUImage *image) {

  if (index >= descriptors.size())
    throw dbg::trace_exception("Input not in range");

  if (descriptors[index].type != IO_IMAGE)
    throw dbg::trace_exception("Wrong input type 'Image'");

  descriptors[index].data.image = image;
  
}

void ComputeModule::setOutputBuffer(size_t index, GPUBuffer *buffer) {

  if (index >= descriptors.size())
    throw dbg::trace_exception("Output not in range");

  if (descriptors[index].type != IO_BUFFER)
    throw dbg::trace_exception("Wrong output type 'Buffer'");

  descriptors[index].data.buffer = buffer;
  
}

void ComputeModule::setOutputImage(size_t index, GPUImage *image) {

  if (index >= descriptors.size())
    throw dbg::trace_exception("Output not in range");

  if (descriptors[index].type != IO_IMAGE)
    throw dbg::trace_exception("Wrong output type 'Image'");

  descriptors[index].data.image = image;
  
}

void ComputeModule::prepare() {

  std::vector<VkDescriptorSetLayoutBinding> layoutBindings;

  for (const IOLayoutInfo & info : getIOLayout()) {
    VkDescriptorSetLayoutBinding binding = {};
    binding.binding = info.binding;
    binding.descriptorCount = info.count;
    binding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    binding.descriptorType = getIOType(info.type);

    layoutBindings.push_back(binding);

    Descriptor desc;
    desc.binding = info.binding;
    desc.count = info.count;
    desc.data = {nullptr};
    desc.type = info.type;
    desc.name = info.name;

    descriptors.push_back(desc);
  }

  descLayout = vkutil::createDescriptorSetLayout(state, layoutBindings);
  uint32_t pushConstantSize = getPushConstantSize();
  if (pushConstantSize) {
    VkPushConstantRange constantRange = {};
    constantRange.offset = 0;
    constantRange.size = pushConstantSize;
    constantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    pipelineLayout = vkutil::createPipelineLayout(state, descLayout, constantRange);
    
  } else {
    pipelineLayout = vkutil::createPipelineLayout(state, descLayout);
  }
  pipeline = vkutil::createComputePipeline(state, shaderModule, pipelineLayout, entryName.c_str());
  descPool = vkutil::createDescriptorPool(state, computePoolSize());
  descSets = vkutil::allocateDescriptorSets(state, descPool, descLayout);
  
}

std::vector<VkDescriptorPoolSize> ComputeModule::computePoolSize() {

  std::vector<VkDescriptorPoolSize> sizes;
  for (const IOLayoutInfo & info : getIOLayout()) {
    VkDescriptorPoolSize size;
    size.descriptorCount = info.count;
    size.type = getIOType(info.type);
    sizes.push_back(size);
  }

  return sizes;
  
}

void ComputeModule::updateDescriptors() {

  std::vector<Descriptor> descriptors = this->getDescriptors();
  std::vector<VkWriteDescriptorSet> writes(descriptors.size());

  size_t i = 0;
  
  for (const Descriptor & desc : descriptors) {

    VkWriteDescriptorSet write = {};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.dstBinding = desc.binding;
    write.dstSet = descSets[0];
    write.descriptorCount = desc.count;

    switch (desc.type) {

    case IO_IMAGE:
      write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
      write.pImageInfo = (VkDescriptorImageInfo *) desc.data.image->getDescriptorInfo();
      break;

    case IO_BUFFER:
      write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
      write.pBufferInfo = (VkDescriptorBufferInfo *) desc.data.buffer->getDescriptorInfo();
      break;

    case IO_UNIFORM_BUFFER:
      write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      write.pBufferInfo = (VkDescriptorBufferInfo *) desc.data.buffer->getDescriptorInfo();
      break;

    default:
      break;
      
    }

    writes[i++] = write;
    
  }

  vkUpdateDescriptorSets(vkutil::getDevice(state), writes.size(), writes.data(), 0, nullptr);
  
}

std::vector<ComputeModule::Descriptor> ComputeModule::getDescriptors() {
  return descriptors;
}

void ComputeModule::setPushConstantPointer(void *ptr) {
  pushConstantValues = ptr;
}

void ComputeModule::execute(VkCommandBuffer & cmdBuffer, uint32_t xCount, uint32_t yCount, uint32_t zCount) {

  vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
  vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipelineLayout, 0, 1, &descSets[0], 0, nullptr);
  if (getPushConstantSize())
    vkCmdPushConstants(cmdBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, getPushConstantSize(), pushConstantValues);
  vkCmdDispatch(cmdBuffer, xCount, yCount, zCount);
  
}

AutoDetectModule::AutoDetectModule(const vkutil::VulkanState & state, std::string srcFileName) : ComputeModule(state) {

  std::vector<uint32_t> moduleData = spirv::loadCode(srcFileName);
  
  parsedLayout = extractLayoutFromShaderCode(moduleData, entryName, &parsedPushConstantSize);
  shaderModule = vkutil::createShaderModule(state, (const char *) moduleData.data(), moduleData.size() * sizeof(uint32_t));
  
}

std::vector<ComputeModule::IOLayoutInfo> AutoDetectModule::getIOLayout() {
  return parsedLayout;
}

uint32_t AutoDetectModule::getPushConstantSize() {
  return parsedPushConstantSize;
}

std::vector<ComputeModule::IOLayoutInfo> AutoDetectModule::extractLayoutFromShaderCode(std::vector<uint32_t> & moduleData, std::string & entryName, uint32_t * pushConstantRange) {

  spirv::Module module = spirv::parse(moduleData);

  std::vector<IOLayoutInfo> layoutInfos;
  uint32_t pushConstantSize;
  
  for (uint32_t varId : module.variableIds) {

    const spirv::Value & value = module.values[varId];

    std::cout << "Storage class " << value.val.var.storageClass << std::endl;

    switch (value.val.var.storageClass) {

    case spirv::STO_Uniform:
    case spirv::STO_UniformConstant:
    case spirv::STO_StorageBuffer:
    {
      IOLayoutInfo info;
      info.count = 1;
      info.name = value.name;

      for (std::shared_ptr<spirv::Decoration> deco : value.decorations) {
	if (deco->type == spirv::DEC_Binding) {
	  info.binding = deco->data[0];
	}
      }

      const spirv::TypePointer * varType = dynamic_cast<const spirv::TypePointer *>(value.val.var.type);
      const spirv::TypeImage * imageType = dynamic_cast<const spirv::TypeImage *>(&(varType->type));
      if (imageType) {
	info.type = IO_IMAGE;
      } else if (varType->storageClass == spirv::STO_UniformConstant) {
	info.type = IO_UNIFORM_BUFFER;
      } else {
	info.type = IO_BUFFER;
      }

      layoutInfos.push_back(info);
      break;
    }

    case spirv::STO_PushConstant:
    {
      std::cout << "PushConstant element: " << value.name << std::endl;
      std::cout << "Type: " << value.val.var.type->name << " size: " << value.val.var.type->getSize() << std::endl;

      pushConstantSize += value.val.var.type->getSize();
    }

    default:
      break;
      
    }
    
  }

  entryName = module.entryPoint;
  *pushConstantRange = pushConstantSize;

  return layoutInfos;
  
}

TestComputeModule::TestComputeModule(const vkutil::VulkanState & state) : ComputeModule(state, "test.comp.spirv") {

}

TestComputeModule::~TestComputeModule() {}

std::vector<ComputeModule::IOLayoutInfo> TestComputeModule::getIOLayout() {
  IOLayoutInfo inputInfo;
  inputInfo.binding = 0;
  inputInfo.count = 1;
  inputInfo.type = IO_IMAGE;

  IOLayoutInfo outputInfo;
  outputInfo.binding = 1;
  outputInfo.count = 1;
  outputInfo.type = IO_IMAGE;
  
  return {inputInfo, outputInfo};
}

uint32_t TestComputeModule::getPushConstantSize() {
  return 0;
}

