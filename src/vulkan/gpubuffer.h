#pragma once

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include "vulkan/util.h"
#include "vulkan/gpuresource.h"

class GPUBuffer : public GPUResource {

public:
  GPUBuffer(const vkutil::VulkanState & state, size_t size);
  GPUBuffer(const vkutil::VulkanState & state, std::vector<uint8_t> data);
  GPUBuffer(const vkutil::VulkanState & state, void * data, size_t size);

  virtual ~GPUBuffer();

  /// Uploads the current data in the transfer buffer to the GPU.
  void uploadToGPU();

  void copyTo(GPUBuffer & dest);
  void copyTo(VkCommandBuffer & cmdBuffer, GPUBuffer & dest);
  
  /// Downloads the data from the GPU and stores it at the given address.
  void getDataFromGPU(void * dest);

  void * mapToMemory();
  void unmapFromMemory();

  VkBuffer getBuffer() const;
  VkDeviceSize getSize() const;

  void * getDescriptorInfo() override;

protected:

  const vkutil::VulkanState & state;
  VkDeviceSize bufferSize;
  VkBuffer buffer;
  VmaAllocation memory;

  VkBuffer stagingBuffer;
  VmaAllocation stagingBufferMemory;

  VkDescriptorBufferInfo descInfo;
  
};
